Project Proposal for EL6483
==========================

## Team

Team members (up to four). Write name and email address of each team member:


1.Sagar Deo(sd2602) 

2.Krishnan Ganesh(kg1682)

3.Puneet Chugh(pc1837)

4.N/A

## Idea

We propose a Do it yourself(DIY) Motion controlled Music Player that can read audio files from a sd card or usb and can be played via a speaker or a headset. The control of the 
player is done using the accelerometer, wherein tilting the board in differnt direction causes the song to change, play-pause or stop based on the requirement.

Most of the projects that use microcontrollers to play audio, use the WAV format that is bulky and has inefficient storage, but we plan to use MP3 that is compressed and consumes
less space, hence more efficient.

Our motivation is to implement a ...do it yourself motion controlled music player.... and to use the full extent of the boards functionality. Our aim is to innovate within certain 
cost boundaries and to have fun while doing it. Another interesting application can be on announcement systems that reproduce a certain recorded message (in subways, elevators, etc.). These systems use a One Time Programmable IC that is expensive and 
have a limited functionality. Our device can work as a low cost replacement of these systems.


## Materials

The materials that would be used in this project is:
1) STM board

2) SD card - This would store the audio. 
Link : http://www.amazon.com/SanDisk-Flash-Memory-Card-SDSDB-002G-A14F/dp/B003AM8A1G

3) SD card slot - This would be used for interfacing the SD card with the board. 
Link : http://www.miniinthebox.com/sd-card-reader-module-for-arduino-uno-2560-avr-arm-pic-socket-spi-slot_p1113821.html


## Milestones

Milestones mark specific points along a project timeline. Your project should have two milestones, and you should plan to demonstrate a working prototype at each:

* 16 April 2015: To play mp3 music and to initialize accelerometer control

* 7 May 2015: To complete implementing the LED matrix visualization 

## Plan of work

The plan of the work has been divided into some important steps:

1st week:
Understanding the implementation of spi communication protocol for our project.
Interfacing sd card/usb with the board.
Ordering the SD card and SD card slot.

2nd week:
Writing and debugging the code module for read/write operation on the SD card.
Harware testing of the sd card ( perforimng the read and write data operation on the card) using FatFs format.

3rd week:
Study the different codec options for decoding the MP3 file.
Implement and debug the codec module in our program.
Sending the data to the audio driver and dac.
Playing the song using a the speaker.

4th week:
Adding the music control functionality (play, pause and next) in the code.
Debug the code and control the music flow using external buttons.

5th week:
Adding the code to implementing the music control with the accelerometer module.
Debug, calibratre and implementing the music control using an accelerometer.

6th week:
Looking into the future scope of integrating a LCD display that shows the details of songs being played. 
Completion of the Project Report and other documentations. 