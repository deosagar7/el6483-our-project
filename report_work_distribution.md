
1) A short summary/abstract of your project (1 paragraph) - "Sagar"

2) A brief introduction, describing and motivating your project (3-6 paragraphs) - "Krishnan"

3) A hardware section: include links to any extra parts you used, and a connection diagram. You should also include a table of pins used and their functions (even if you didn't use external parts) "Krishnan(include diagrams with a http link that uses ![]) "

4) A software section: you must credit any third-party software you have used in your project. This includes, but is not limited to, any of the following: the standard peripheral library, any RTOS you used, third-party libraries for extra peripherals (whether you used them as-given or modified them), example code you found online for using on-board peripherals, etc. For each piece of code you used that you didn't write yourself from scratch, you should link to the original source and explain what modifications you had to make (if any) to integrate it into your project. "Puneet Chugh"

5) Instructions for how to use your project - "Puneet Chugh(pictures to be included)"

6) A discussion section: describe your general approach to implementing your project, discuss any problems or issues you faced and how you overcame them, etc. (6-12 paragraphs) -"Krishnan and Sagar"

7) A results section: include images, plots, and/or tables related to system performance. For example, if you have implemented a pedometer, you should measure its accuracy at different walking speeds; if you are using gesture recognition, you should measure its accuracy for several different users; if you are using pitch recognition to open a lock, you should measure the tolerance of your system (in Hz), etc. -"Puneet and Sagar"

8) A conclusion section (1-2 paragraphs) summing up your work- "Puneet Chugh"