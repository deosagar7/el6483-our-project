A Project Report on Motion Controlled MP3 player with Audio visualization
==========================================================================
- by Sagar Deo, Puneet Chugh and Krishnan Ganesh

##ABSTRACT:

A DIY MP3 player has been implemented using a STM 32 F4 Discovery board. The implementation has an added functionality of motion control and audio visualization. The audio visualization is implemented on a Bi-color 8x8 LED matrix. The system reads MP3 music files stored on a pen drive and decodes it using the Helix MP3 decoder before playing it using the on board audio DAC and amplifier. The advantage of using MP3 music format over a raw audio file is that it offers efficient data storage. The on board accelerometer is used to control the audio playback according to the board’s orientation. The user can play the next song, replay the current song, pause music or increase and decrease the music volume by changing the orientation of the board.

##INTRODUCTION:
This is a do it yourself music player with motion control and visualization. We use the STM32F4 board which has an on board accelerometer,DAC,ADC,Temperature sensor etc. This micro controller is based on the ARM7 processor and it supports USART,SPI and I2C data transfer modes. In this project we use the accelerometer to achieve certain operations like play, pause etc based on the orientation of the board. We use I2C to communicate with an 8x8 led matrix that we use for visualization of  the song. The song  is played from a USB attached to the board

Our aim was to design a low-cost music player that had as much or even more functionality than any other modern music player ,and also to use the full potential of the board and to have fun while doing it. Another interesting application can be on announcement systems that reproduce a certain recorded message (in subways, elevators, etc.). These systems use a One Time Programmable IC that is expensive and have limited functionality. Our device can work as a low cost replacement of these systems.

##HARDWARE SECTION:

The STM32F407VGT6 micro controller features a 32-bit ARM cortex M4 ,it has 1 MB of flash memory and 192KB RAM. It contains the following

* Two ST MEMS digital accelerometer
* Digital microphone
* one audio DAC with integrated class D speaker driver
* LEDs and push buttons
* USB OTG micro-AB connector.


The Adafruit bicolour 8x8 ledmatrix,(http://www.adafruit.com/product/902)\

They have 64 red and 64 green LEDs inside, for a total of 128 LEDs controlled as a 8x16 matrix. This backpack solves the annoyance of using 24 pins or a bunch of chips by having an I2C constant-current matrix controller sit neatly on the back of the PCB. The controller chip takes care of everything, drawing all 128 LEDs in the background. All you have to do is write data to it using the 2-pin I2C interface.


These are the two main components in this project and these are connected as shown in a roughly hand drawn diagram
![CONNECTION DIAGRAM](http://oi57.tinypic.com/2r3ia3d.jpg)


we need a microUSB to USB connector to connect the USB to the board and a mini USB cable to connect the board to the computer

##SOFTWARE SECTION:

The software developed is an integrated product of initialization modules of led matrix, I2C and SPI communication modules, MP3 decoder,etc.

Next part specifies  the Ports that are being used for interfacing with different modules:

1) `Port A` – `Pin 4` for I2S communication to send the music data to audio driver using I2S communication protocol

2) `Port B` – `Pin 6` and `Pin 9` are used for I2C1 – Send the data to codec

   `Pin 10` and `Pin 11` are used for I2C2 – Send the data to audio visualizer.

3) `Port C` - `Pin 7`, `Pin 10` and `Pin 12` for MCK, SCK and SD pins os SPI3

Given below is the flow of the software designed for our project.

![Flow Diagram of the code for `Motion Controlled MP3 player using visualization`](http://i.imgur.com/LnNqJeM.png)

We would like to express our gratitude to Benjamin for the [code](http://vedder.se/2012/12/stm32f4-discovery-usb-host-and-mp3-player/) . We have made use of the code with some rigorous changes.

##INSTRUCTIONS FOR USING THE PROJECT

The entire code of `Motion Controlled Mp3 Player with Visualization` exists under the root directory  STM32F4_USB_MP3_working including all the .c, libraries and header files under various subdirectories. Once you have the STM board and LED 8*8 matrix with you, you are only few steps away from experiencing good quality music being played through the STM board.

The hardware arrangements are very simple. The LED 8*8 matrix uses I2C2 to visualize music on the LED matrix. It has four pins as follows:

```
   I2C2	       |  STM board
-----------------------------------------------------------------------
 1) Vcc        |    Vcc
------------------------------------------------------------------------
 2) Gnd		   |    Gnd
------------------------------------------------------------------------
 3) SCLK	   |    PB10
------------------------------------------------------------------------
 4) SDA		   |    PB11
```

Connect the Flash drive(formatted to FAT32 format) containing mp3 music files to the female connector attached to the STM board via micro-usb at the other end.

Now to boot the board with the code follow the following steps:

1) Enter the main directory  STM32F4_USB_MP3_working. This directory has a script file named `Makefile`. Makefile is used for automating the operations, that we will perform in the next steps.

2) Run 'make clean' (to clear any previous instance of object file or executable file)

3) Run 'make' (to create the executables)

4) Run 'make flash'(to load the code on the board)

Wait until the terminal prints “jolly good !!”

Once you see this message, the board is ready to be used as a music player. Connect a headset/earphones to the 3.5 mm jack present on the board to listen to music.

Directions to use the motion control feature:(Hold the board with the usb facing away from you and)

1) `Play the next song` – Tilt the board toward right and bring back to normal position. The next song would be played. Note: The next song would not be played until you bring the board back.

2) `Replay the song` – The same song can be replayed if you tilt the board left .

3) `Change volume level` – The volume can be increased or decreased if we tilt the board forward or backward, respectively.

4) `Pause the song` – The user button(Blue Button) can be used to pause the song for the duration you keep it pressed.

So, all these features make our product more user friendly and lend it the ability to be used with ease.

##DISCUSSION:

The project implementation can be broadly classified into the following steps:

###1. Playing the MP3 music file from the pen drive.

The code to configure the board to read data from the pen drive and decode it using the Helix MP3 decoder can be found [here](http://vedder.se/2012/12/stm32f4-discovery-usb-host-and-mp3-player/). All the make files had to be modified to burn the code on board.

###2. Implementing motion control.

A code to initialize and configure the accelerometer was developed and integrated with the MP3 player module. The accelerometer module parameters were tuned to perform the above mentioned functions. In order to control playback we had to analyze the entire MP3 player module and develop our code accordingly. A deeper analysis of the code using GDB revealed that in order to repeat the song we had to manipulate the structure-object “dir” of “DIR” structure. So in order to control the music flow, we copied this "dir" into another DIR object "pre_dir" and pointed towards "pre_dir".

Following are a few GDB screen shots that show the value of the "dir" object for different songs in the pendrive.

The value for same songs:

![song1](http://i.imgur.com/ysclPWJ.png)
![song1](http://i.imgur.com/ZHvB3iN.png)

The value for different songs:

![song2](http://i.imgur.com/IKBEwLp.png)
![song3](http://i.imgur.com/0Nel4v1.png)

###3. Testing the LED matrix by displaying data on the board.

The 8x8 LED matrix uses I2C communication protocol to communicate with the microcontroller. An I2C code was developed to test the matrix. Even after the availability of the HT16K33 LED driver, this step turned out to be quite challenging. The data transfer to the matrix was unsuccessful and we had to use gdb to go through the code line by line in order to zero in on the error. After going through the code line by line we realized that the GPIO pin on the board did not require a pull up resistor for communication with the LED matrix, and we were not configuring the GPIO pin properly. We reached this conclusion after observing the flag values in the status registers.

![](http://i.imgur.com/Uqqf9ft.jpg)

###4. Integrating the LED module with the Board.

The LED driver and matrix were soldered and physically integrated with the board.

###5. Integrating the I2C code with MP3 player module.

Since the MP3 module was also using the I2C communication protocol to read the data from the USB port, we had to choose the SDA and SCL pins accordingly. The biggest challenge we faced while integrating the LED matrix code module to the MP3 module was the fact that in order for the MP3 decoder to run at all, there were strict timing constraints and missing a deadline led to the stopping of code execution. Therefore we had to devise some workarounds in our LED matrix data transfer module. For instance we couldn’t stop the I2C communication once the MP3 decoder started executing. This was a major concern since any meaningful data display on the LED matrix required the I2C communication to restart whenever a playback control command was to be executed. The restart was essential because after every restart the pointer to the LED matrix’s display register, which was incremented after every write operation on the register, was reset to the original position i.e. at the start of the first row. Our workaround was to develop a piece of code that kept a track of all the instances the write operation was being done, and then use that information to manually update the position of the data register pointer to its default value before executing the playback control. Moreover, due to a lack of library support for the LED driver we had to manually design all the graphics to be displayed on the LED matrix.

###6. Creating graphics for display on the LED matrix.

The mono output decoded LR interleaved music data was used to generate the visualization graphics for the LED matrix display.

##RESULTS:

A short YouTube video demonstrating the working of our project is attached below. Click on the thumbnail below to reach the video.

[![ScreenShot](http://i.imgur.com/tjAx1mK.png)](https://www.youtube.com/watch?v=5Q3ypSu4obc)

As it can be seen in the video, we were able to successfully develop a motion controlled music player with a visualizer. Here are a few images detailing the different graphic displayed on the LED Matrix.

# When the user button is pressed the music pauses
![](http://i.imgur.com/EyaugkS.jpg)

# An image of the visualizer at work
![](http://oi57.tinypic.com/11siefm.jpg)

# Play the next song!!
![](http://i.imgur.com/rGEf7fr.jpg)

##CONCLUSION:
The project Motion Controlled mp3 player with visualization was satisfactorily completed in a span of 5-6 weeks. The product meets its objectives.  And both the timelines were adhered to. The product has features of motion control and visualization that make it even fun to play with.

We first fetch mp3 data from the usb flash drive into the arm processor which has a decoder in the form of a software in processor. The processor feeds the raw data to audio DAC on board to further feed the speaker through the 3.5mm jack to play the audio. The visualizer continuously displays the music data while the song is being played and also displays some cool graphics when making use of the  motion controlled features.

We would like to thank `Fraida Fund` for her persistent help and advice in this project.

##FUTURE SCOPE:
We have future plans of expanding this project to a video player. That will require synchronizing audio and pictures. We might end up using a Real-Time OS for that purpose.
